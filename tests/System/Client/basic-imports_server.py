#! /usr/bin/env python
###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
""" Just importing stuff that should be present
"""
# pylint: disable=unused-import,import-error

import pyparsing
import XRootD
import gfal2
import stomp
import requests

# import futures
import certifi
import pexpect
import fts3
import LbPlatformUtils

from distutils.spawn import find_executable


cmds = ["voms-proxy-init", "voms-proxy-info"]

for cmd in cmds:
    res = find_executable(cmd)
    if not res:
        raise RuntimeError()

for cmd in ["condor_submit", "condor_history", "condor_q", "condor_rm", "condor_transfer_data"]:
    res = find_executable(cmd)
    if not res:
        raise RuntimeError(f"No {cmd}")

res = find_executable("ldapsearch")
if not res:
    raise RuntimeError()
